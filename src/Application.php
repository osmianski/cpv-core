<?php

namespace Vlaosm\CpvCore;

use Symfony\Component\Console\Application as BaseApplication;

class Application extends BaseApplication
{
    public function getName() {
        return 'Composer Package Versioning Demo';
    }

    /**
     * Reads currently installed version from `composer.lock` file
     *
     * @return string|null
     */
    public function getVersion() {
        if (!($json = json_decode(file_get_contents(BP . '/composer.lock'), true))) {
            return null;
        }

        foreach ($json['packages'] ?? [] as $package) {
            if ($package['name'] === 'vlaosm/cpv-core') {
                return $package['version'];
            }

        }
        return null;
    }

    /**
     * Add command-line commands to this application registered in extra->cpv->commands section in `composer.json`
     * files of library packages
     *
     * @return $this
     */
    public function configure() {
        foreach (glob(BP . '/vendor/*/*/composer.json') as $filename) {
            if (!($json = json_decode(file_get_contents($filename), true))) {
                continue;
            }

            foreach (($json['extra']['cpv']['commands']) ?? [] as $name => $class) {
                $this->add(new $class($name));
            }
        }

        return $this;
    }
}