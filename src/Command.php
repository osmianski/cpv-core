<?php

namespace Vlaosm\CpvCore;

use Symfony\Component\Console\Command\Command as BaseCommand;

class Command extends BaseCommand
{
    public function generate($filename, $template, $variables = []) {
        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0777, true);
        }

        file_put_contents($filename, $this->render($template, $variables));
    }

    protected function render($__template, $__variables = []) {
        list($__vendor, $__package, $__template) = explode('/', $__template, 3);
        if (!is_file($__filename = BP . "/vendor/{$__vendor}/{$__package}/templates/{$__template}.php")) {
            throw new \Exception("Template '{$__filename}' not found");
        }

        extract($__variables);
        ob_start();

        /** @noinspection PhpIncludeInspection */
        include $__filename;

        return ob_get_clean();
    }
}